﻿//-----------------------------------------------------------------------
// <copyright file="ImageSchemaTest.cs" company="B'15, IIT Palakkad">
//      Open Source. Feel free to use the code, but don't forget to acknowledge. 
// </copyright>
// <author>
//      vishal kumar chaudhary
// </author>
// <review>
//      not reviewed
// </review>
//-----------------------------------------------------------------------

namespace Masti.Schema
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;

    /// <summary>
    /// Testing the functionality of imageSchema Encode function and Decode function
    /// </summary>
    public class ImageSchemaTest
    { 
        /// <summary>
        /// Tests the cases described in path for image encoding and decoding
        /// </summary>
        /// <param name="path">Path to test data file </param>
        /// <returns>true if all test cases are checked</returns>
        public bool Run(string path)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            // Console.WriteLine("Testing start");
            string[] lines = File.ReadAllLines(path: path);
            int lineNumber = 1;
            int numberOfTests = int.Parse(lines[0], CultureInfo.CurrentCulture);

            // reading for each test cases 
            while (numberOfTests > 0)
            {
                numberOfTests -= 1;
                int numOfKeys = int.Parse(lines[lineNumber], CultureInfo.CurrentCulture);
                lineNumber += 1;

                Dictionary<string, string> testDictionary = new Dictionary<string, string>();
                //// adding all string to string tags
                while (numOfKeys > 0)
                {
                    numOfKeys -= 1;
                    string[] keyValue = lines[lineNumber].Split(':');
                    lineNumber += 1;
                    testDictionary.Add(keyValue[0], keyValue[1]);
                }
                
                numOfKeys = int.Parse(lines[lineNumber], CultureInfo.CurrentCulture);
                lineNumber += 1;
                //// adding all string to image_string tags
                while (numOfKeys > 0)
                {
                    numOfKeys -= 1;
                    string[] keyValue = lines[lineNumber].Split(':');
                    lineNumber += 1;
                    path = "C:/Users/Akshat/pse-project/Schema/";
                    byte[] imageBytes = File.ReadAllBytes(path + keyValue[1].Replace(" ", string.Empty));
                    testDictionary.Add(keyValue[0], Convert.ToBase64String(imageBytes));
                }
             
                ImageSchema imageSchema = new ImageSchema();
                //// encoding data with help of imageSchema Encode function
                string encodedata = imageSchema.Encode(testDictionary);
                //// decoding data with help of imageSchema Encode function
                Dictionary<string, string> decodedDictionary = new Dictionary<string, string>(imageSchema.Decode(encodedata, false));
                //// checking all the key value pair are same or not for full decoding
               
                int breakFlag = 0;
                foreach (string key in testDictionary.Keys)
                {
                    if (!(decodedDictionary.ContainsKey(key) & decodedDictionary[key] == testDictionary[key]))
                    {
                        // Console.WriteLine("Failed");
                        breakFlag = 1;
                        break;
                    } 
                }
                 
                if (breakFlag == 0)
                {
                    // Console.WriteLine("Passed");
                }
            }

            return true;
        }
    }
}
