# Managerial Report

_Ashutosh Upadhye (111501029)_

## Timeline
|Date      |What's due                   ||
|----------|-----------------------------||
|Sep 16    |Design Specs                 ||
|Oct 12    |Individual Component code    ||
|Nov 02    |Final Project demo           ||

## Version Control

Had two options, decided to go with Bitbucket. 

### GitHub

- Decent Version Control.
- _Owned_ by MicroSoft.

### Bitbucket

- All the functionalities provided by GitHub plus a lot more.
- **Leader board** (Trello's): Lists of To Do, Doing and Done. Objects can be moved from one list to another.
- Option to add reviewers, who would approve pull requests.
- **Open Source**.

## Hierarchy

- Each team lead will fork the repo. Find it [here](https://bitbucket.org/ashutosh2411/pse-project/).
- Only the pull requests from the team leads will be entertained.
- Architect will be added as collaborator.
- Q&A members will be added as reviewers.
- Team leads can decide to have an independent hierarchy amongst their team members (Add them as collaborators or ask them to fork, choice is theirs).

## Meetings 

- Set the deadlines after consulting with the Architect and the Team Leads.
- Will push the team to meet the deadlines.
- If a team is under performing, take appropriate measures to motivate and help them. 
- I shall keep updated with all the team leads through Trello and Slack.

## Scheduling
### Trello's Leaderboard:

- Bitbucket has an integrated Trello leaderboard which allows users to keep a track of the project. 
- A roll-up scheduler will be created, _either in trello or an alternative will be looked for._ 
- Final decision on scheduler will be taken, and roles concerning the scheduler will be taken later. 
- The Dashboard for MASTI's leaderboard could be found [here](https://trello.com/b/TvY7UPVR).

## Conflict resolution

- Analysing pros and cons of proposed ideas and coming to a conclusion unanimously. 

## Logs and Records

- I plan to maintain the log of meetings, just so that the ideas that have been discussed don't disappear in thin air, although we go with a different proposal.
- Maintain a To Do list, with the help of the Architect. _in Trello_

## Miscellaneous

- Ensuring a unison in the team.
- Keeping in touch with the client, (Prof in this case).