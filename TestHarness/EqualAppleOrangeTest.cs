﻿//-----------------------------------------------------------------------
// <author> 
//     Adrian McDonald Tariang (adrian47mcdonald@gmail.com)
// </author>
//
// <date> 
//     27th October, 2018
// </date>
// 
// <reviewer>
//      Libin N George (libinngeorge@gmail.com)
// </reviewer>
// 
// <copyright file="EqualAppleOrangeTest.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      Example test for the Test Harness. 
// </summary>
//-----------------------------------------------------------------------

namespace Masti.TestHarness
{
    using Masti.QualityAssurance;

    /// <summary>
    /// Define a Test to check if Apples and Oranges are fruits.
    /// </summary>
    public class EqualAppleOrangeTest : ITest
    {
        /// <summary>
        /// The logger is used for Logging functionality within the test.
        /// Helps in debugging of tests.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="EqualAppleOrangeTest" /> class.
        /// </summary>
        /// <param name="logger">Assigns the Logger to be used by the Test</param>
        public EqualAppleOrangeTest(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Describe the Comparison test defined by the module developer.
        /// </summary>
        /// <returns>Returns whether the status is successful or not.</returns>
        public bool Run()
        {
            string apple = "Fruit";
            string orange = "Fruit";

            this.logger.LogInfo("Performing Comparison...");
            this.logger.LogWarning("Caution! Step Away from the Machine\n");

            // Perform Test Comparison.
            if (apple.Equals(orange))
            {
                this.logger.LogSuccess("Success!");
                return true;
            }
            else
            {
                this.logger.LogError("Failed!");
                return false;
            }
        }
    }
}
