﻿// -----------------------------------------------------------------------
// <author> 
//      Ayush Mittal (29ayush@gmail.com) 
// </author>
//
// <date> 
//      12-10-2018 
// </date>
// 
// <reviewer>
// Rajat Sharma
// </reviewer>
//
// <copyright file="DataOutgoing.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
//      You are allowed to use the file and/or redistribute/modify as long as you preserve this copyright header and author tag.
// </copyright>
//
// <summary>
//      This File contains the Data Outgoing Component.
//      It checks for data in queue by polling.If queue has a message, send it to its designation.
//      This file is a part of Networking Module.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// Following partial class implements DataOutGoing Component - 
    /// Function to send data from the queue over the network.
    /// Constructors to start this and receiving component.
    /// </summary>
    public partial class Communication : ICommunication, IDisposable 
    {
        /// <summary>
        /// lockObjects,lockStatus,ConnectedClients are Dictionaries accessed by multiple threads, 
        /// any addition/deletion to these tables is thus made thread safe by using this lock.
        /// </summary>
        private readonly object mainLock = new object();

        /// <summary>
        /// acknowledgeEvents is a Dictionary accessed by multiple threads, 
        /// any addition/deletion to this tables is thus made thread safe by using this lock.
        /// </summary>
        private readonly object ackLock = new object();

        /// <summary>
        /// It stores IP of professor.( On professor's machine it's basically the local IP. )
        /// </summary>
        private IPAddress ipAddress;

        /// <summary>
        /// It stores port at which professor is listening.
       /// </summary>
        private int port;

        /// <summary>
        /// On student machine, it stores whether the socket to professor is working or not.
        /// On Professor machine, it stores whether the professor is listening or not.
        /// Purpose :-
        ///   -> Any DataStatus Failure notification can be considered a temporary, and Communication module
        ///      need not be restarted until this attribute is false.
        /// Note  :- 
        ///   -> Before the connection is initiated to professor (first message is sent) this remains false.
        /// </summary>
        private bool isRunning = false;

        /// <summary>
        /// Stores the listening socket for professor, nothing for student.
        /// </summary>
        private TcpListener serverSocket = null;

        /// <summary>
        /// No of times to send a message in total ( message is resent if acknowledgement is not received).
        /// </summary>
        private int maxTries;

        /// <summary>
        /// Time to wait for recipient to send acknowledgement in milliseconds.
        /// </summary>
        private int waitTimeForAcknowledge;

        /// <summary>
        /// Local Variable to Store if this module is instantiated as professor or student.
        /// </summary>
        private bool isStudent = false;

        /// <summary>
        /// Can be used when testing using threads to identify log corresponding to various threads.
        /// </summary>
        private IPAddress testIP = null;

        /// <summary>
        /// Event used to manage wait in SendFromQueue function.
        /// </summary>
        private AutoResetEvent queueEvent;

        /// <summary>
        /// Event used to manage stop the MainThread.
        /// </summary>
        private ManualResetEvent stopEvent;

        /// <summary>
        /// Objective :: Keep a reference to existing connection(socket) corresponding to an IP:Port.
        /// Key       :: IPAddress.
        /// Value     :: Socket.
        /// </summary>
        private IDictionary<IPAddress, Socket> connectedClients;

        /// <summary>
        /// Objective :: When a message is sent, it is added to this table, It tracks the messages whose acknowledgement is currently not processed.
        /// TODO      :: Alter this methodology to, a new thread which keeps track of all acknowledgement(table) at regular interval
        /// Key       :: MD5Hash of message + (not hashed)IPEndPoint. 
        /// Value     :: AutoResetEvent .
        /// Note      :: All Messages are supposed to be unique, since schema encodes the timestamps in message.
        /// </summary>
        private IDictionary<string, AutoResetEvent> acknowledgeEvents;

        /// <summary>
        /// Objective :: Since multiple threads are spawned to send messages (Only one queue) over the network with same source
        ///           :: to prevent different threads to send overlapping messages locks are used so that only one thread 
        ///           :: sends the message at a time.
        /// Key       :: IPEndPoint. 
        /// Value     :: Object to be used as reference for locks.
        /// </summary>
        private IDictionary<IPEndPoint, object> lockObjects;

        /// <summary>
        /// Objective :: We use a double checked lock mechanism, it stores true/false check corresponding to the locks.
        /// Key       :: IPEndPoint. 
        /// Value     :: Bool to be used as check for locks.
        /// </summary>
        private IDictionary<IPEndPoint, bool> lockStatus;

        /// <summary>
        /// Reference to thread which runs the a function to pop data from queue and process it.
        /// </summary>
        private Thread sendingThread = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="Communication"/> class.
        /// This constructor is used to initialize common data.
        /// </summary>
        public Communication()
        {
            this.connectedClients = new ConcurrentDictionary<IPAddress, Socket>(); // IP , Socket
            this.acknowledgeEvents = new ConcurrentDictionary<string, AutoResetEvent>(); // MESSAGEEHASH + IP , AutoResetEvent
            this.lockStatus = new ConcurrentDictionary<IPEndPoint, bool>(); // IP , Bool
            this.lockObjects = new ConcurrentDictionary<IPEndPoint, object>(); // IP, Obj
            this.schema = new ImageSchema();
            this.queueEvent = new AutoResetEvent(false);
            this.stopEvent = new ManualResetEvent(true);
            this.networkTelemetryCollector = TelemetryCollector.Instance;
            this.networkTelemetryCollector.RegisterTelemetry("NetworkTelemetry", new NetworkTelemetry());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Communication"/> class.
        /// This constructor is supposed to be used when instantiating the module for a Professor with a local IP.
        /// </summary>
        /// <param name="port">Port to start the listening server on</param>
        /// <param name="maxRetries">No of times to resend a message if acknowledgement is not received.</param>
        /// <param name="waitTimeForAcknowledge">Time to wait for recipient to send acknowledgement in seconds.</param>
        /// <param name="testIP"> For testing purposes.Bind to specific loop back address.(Read Test File For More) </param>
        public Communication(int port, int maxRetries, int waitTimeForAcknowledge, IPAddress testIP) : this()
        {
            MastiDiagnostics.LogInfo(Helper.ContextLogger("Communication Object Created On Professor Machine" + testIP, 0));
            this.port = port;
            if (maxRetries < 0 || maxRetries == int.MaxValue)
            {
                MastiDiagnostics.LogInfo(string.Format("Input a valid maxRetries"));
                throw new InvalidOperationException();
            }
            else
            {
                this.maxTries = maxRetries + 1;
            }  
            
            if (waitTimeForAcknowledge < int.MaxValue / 1000) 
            {
                this.waitTimeForAcknowledge = waitTimeForAcknowledge * 1000;
            }
            else
            {
                MastiDiagnostics.LogInfo(string.Format("Input a valid waitForAcknowledgement"));
                throw new InvalidOperationException();
            }

            this.testIP = testIP;
            this.Start(); // TODO :: Change the structure so that function isn't called inside constructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Communication"/> class.
        /// This constructor is supposed to be used when instantiating the module for a Professor.
        /// </summary>
        /// <param name="port">Port to start the listening server on</param>
        /// <param name="maxRetries">No of times to resend a message if acknowledgement is not received.</param>
        /// <param name="waitTimeForAcknowledge">Time to wait for recipient to send acknowledgement in seconds.</param>
        public Communication(int port, int maxRetries, int waitTimeForAcknowledge) : this(port, maxRetries, waitTimeForAcknowledge, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Communication"/> class.
        /// This constructor is supposed to be used when instantiating the module for a Student with a local IP.
        /// </summary>
        /// <param name="ipAddress">IP address of professor's machine.</param>
        /// <param name="port">Port of professor's machine</param>
        /// <param name="maxRetries">No of times to resend a message if acknowledgement is not received.</param>
        /// <param name="waitTimeForAcknowledge">Time to wait for recipient to send acknowledgement.</param>
        /// <param name="testIP"> For testing purposes.Bind to specific loop back address.(Read Test File For More). </param>
        public Communication(IPAddress ipAddress, int port, int maxRetries, int waitTimeForAcknowledge, IPAddress testIP) : this()
        {
            MastiDiagnostics.LogInfo(Helper.ContextLogger("Communication Object Created On Student Machine" + testIP, 0));
            this.ipAddress = ipAddress;
            this.isStudent = true;
            this.port = port;
            if (maxRetries < 0 || maxRetries == int.MaxValue)
            {
                MastiDiagnostics.LogInfo(string.Format("Input a valid maxRetries"));
                throw new InvalidOperationException();
            }
            else
            {
                this.maxTries = maxRetries + 1;
            }

            if (waitTimeForAcknowledge < int.MaxValue / 1000) 
            {
                this.waitTimeForAcknowledge = waitTimeForAcknowledge * 1000;
            }
            else
            {
                MastiDiagnostics.LogInfo(string.Format("Input a valid waitForAcknowledgement"));
                throw new InvalidOperationException();
            }

            this.testIP = testIP;
            this.Start(); // TODO :: Change the structure so that function isn't called inside constructor
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Communication"/> class.
        /// This constructor is supposed to be used when instantiating the module for a Student.
        /// </summary>
        /// <param name="ipAddress">IP address of professor's machine.</param>
        /// <param name="port">Port of professor's machine</param>
        /// <param name="maxRetries">No of times to resend a message if acknowledgement is not received.</param>
        /// <param name="waitTimeForAcknowledge">Time to wait for recipient to send acknowledgement.</param>
        public Communication(IPAddress ipAddress, int port, int maxRetries, int waitTimeForAcknowledge) : this(ipAddress, port, maxRetries, waitTimeForAcknowledge, null)
        {
        }

        /// <summary>
        /// Starts the receiving and sending part of the module.
        /// </summary>
        /// <returns> true if there were no problems with initialization(like port is free.) otherwise false. </returns>
        public bool Start()
        {
            if (this.isRunning || this.sendingThread != null)
            {
                // This function shouldn't be called when the module is already running.
                throw new InvalidOperationException();
            }

            if (!this.isStudent)
            {
                try
                {
                    this.StartReceiver().Start();
                }
                catch
                {
                    MastiDiagnostics.LogInfo(Helper.ContextLogger($"Tried Initializing on Port {this.port} which wasn't free", 0));
                    throw;
                }
            }

            // To avoid thread persistance even after exiting of Main Thread
            // Some time 
            this.sendingThread = new Thread(this.SendFromQueue)
            {
                IsBackground = true
            };

            // When running multiple threads on same device (for testing) or any other purpose, it serves well for us
            // to be able to distinguish different threads(objects).
            if (this.isStudent)
            {
                this.sendingThread.Name = "MainSendThread : " + this.testIP;
            }
            else
            {
                this.sendingThread.Name = "MainSendThread : Professor";
                this.isRunning = true;
            }

            this.sendingThread.Start();
            MastiDiagnostics.LogInfo(Helper.ContextLogger($"Module({this.testIP}) isStudent : {this.isStudent} Started Properly {this.ipAddress} {this.port}", 0));
            return true;
        }

        /// <summary>
        /// Stops all the sockets and main threads.
        /// </summary>
        public void Stop()
        {
            this.isRunning = false;
            if (this.serverSocket != null)
            {
                this.serverSocket.Stop();
            }

            // Close all clients sockets to terminate their threads
            foreach (Socket client in this.connectedClients.Values)
            {
                client.Close();
            }

            this.stopEvent.Reset();
            this.queueEvent.Set();
        }
                
        /// <summary>
        /// This method is used to dispose queueEvent,stopEvent by implementing IDisposable
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose Overload following the dispose pattern.
        /// </summary>
        /// <param name="disposing">true if called by Disposes false if called by finalizer</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.stopEvent.Reset();
                this.queueEvent.Set();

                // dispose managed resources
                this.queueEvent.Close();
                this.stopEvent.Close();
                foreach (var ackEvent in this.acknowledgeEvents) 
                {
                    ackEvent.Value.Close();
                }
            }

            // free native resources
        }

        /// <summary>
        /// The send function of the interface adds the messages to in the queue, 
        /// It waits on queueEvent when it is not able to dequeue.
        /// </summary>
        private void SendFromQueue()
        {
            Socket clientSocket;
            while (true)
            {
                // TryDequeue returns false if dequeue was unsuccessful 
                // If successful it returns true and stores result in currentRequest.
                if (!SendRequestQueue.TryDequeue(out Packet currentRequest))
                {
                    MastiDiagnostics.LogInfo(Helper.ContextLogger("Queue Empty, sending the reset signal", 5));
                    try
                    {
                        this.queueEvent.WaitOne();
                        if (!this.stopEvent.WaitOne(10))
                        {
                            return;
                        }
                    }
                    catch (ObjectDisposedException)
                    {
                        MastiDiagnostics.LogError(Helper.ContextLogger("Main Object was already Disposed", 1));
                        return;
                    }

                    continue;
                }

                // Below conditions results in false if there isn't any existing connection to target.
                if (this.connectedClients.ContainsKey(currentRequest.TargetIPAddress))
                {
                    clientSocket = this.connectedClients[currentRequest.TargetIPAddress];
                    this.SendHelper(currentRequest.Data, 0, clientSocket, 0);
                }
                else
                {
                    MastiDiagnostics.LogInfo(Helper.ContextLogger("Connection For EndPoint " + currentRequest.TargetIPAddress + "Not Found.", 5));
                    if (this.isStudent)
                    {
                        if (!currentRequest.TargetIPAddress.Equals(this.ipAddress))
                        {
                            // If the first message doesn't match IP it has to throw exception, otherwise there's no way of knowing whether
                            // was caused due to network error or internal error. (See the working of this.isRunning ).
                            MastiDiagnostics.LogError(Helper.ContextLogger("The Professor Device IP Address Doesn't Match with Target IP Address.", 0));
                            this.DataStatusNotify(currentRequest.Data, StatusCode.Failure);
                        }

                        if (this.InitiateConnection())
                        {
                            MastiDiagnostics.LogInfo(Helper.ContextLogger("Connection Initiated to Prof. at IP:" + this.ipAddress + " Port:" + this.port + "\n", 2));
                            this.SendHelper(currentRequest.Data, 0, (Socket)this.connectedClients[currentRequest.TargetIPAddress], 0);
                            continue;
                        }
                    }

                    this.DataStatusNotify(currentRequest.Data, StatusCode.Failure); // failure callback
                }
            }
        }

        /// <summary>
        /// Sends a given message by formatting it :-
        ///     Every message sent over network has format (prefix = length + ackBit) + message bytes
        ///     length in prefix excludes the bytes used for length and reservedBits(ackBit)
        /// </summary>
        /// <param name="message">The message to be sent</param>
        /// <param name="tries">Tells which try is this to send the message</param>
        /// <param name="socket">The socket to which send the data on</param>
        /// <param name="isAck">Tells if the message being sent is acknowledgment.</param>
        private void SendHelper(string message, int tries, Socket socket, int isAck)
        {
            // Create a new state object.
            ClientState currentState = new ClientState
            {
                Socket = socket
            };
            if (currentState.IPPort == null)
            {
                MastiDiagnostics.LogError(Helper.ContextLogger("Called with null socket, not supposed to happen", 1));
                throw new InvalidOperationException();
            }

            byte[] byteData = Encoding.ASCII.GetBytes(message);
            byte[] lengthBytes = Helper.GetBytes(byteData.Length);
            byte[] ackBits = new byte[] { (byte)isAck };

            // Every message sent over network has format (prefix = length + ackBit) + message bytes
            // length in prefix excludes the bytes used for length and reservedBits(ackBit)
            byte[] prefix = Helper.Combine(lengthBytes, ackBits);

            currentState.Tries = tries;
            currentState.DataSent = 0;
            currentState.Message = message;
            currentState.SetDataToSend(Helper.Combine(prefix, byteData));

            MastiDiagnostics.LogInfo(Helper.ContextLogger("Waiting To Acquire Lock IP EndPoint" + Helper.GetEndPoint(socket), 7));

            // We maintain locks per socket so that more than one thread don't send at 
            // the same time on same socket, otherwise data would overlap.
            while (!this.GetLock(currentState.IPPort))
            {
            }

            MastiDiagnostics.LogInfo(Helper.ContextLogger("Lock Acquired. Calling BeginSend EndPoint" + Helper.GetEndPoint(socket), 5));
            lock (this.ackLock)
            {
                if (!this.acknowledgeEvents.ContainsKey(Helper.MD5Hash(message) + currentState.IPPort))
                {
                    this.acknowledgeEvents.Add(Helper.MD5Hash(message) + currentState.IPPort, new AutoResetEvent(false));
                }
            }

            // If the socket breaks in between this will throw an exception.
            try
            {
                socket.BeginSend(currentState.DataToSend(), 0, currentState.DataToSend().Length, 0, new AsyncCallback(this.SendCallback), currentState);
            }
            catch (SocketException e)
            {
                MastiDiagnostics.LogError(Helper.ContextLogger("SocketError EndPoint :: " + e.ToString() + " " + currentState.IPPort, 0));  // Socket Failed
                this.ResetClient(currentState.IPPort); // Remove any lock-objects,sockets,status bit associated to failed socket.
                lock (this.ackLock)
                {
                    this.acknowledgeEvents[Helper.MD5Hash(currentState.Message) + currentState.IPPort].Close();
                    this.acknowledgeEvents.Remove(Helper.MD5Hash(currentState.Message) + currentState.IPPort);                    
                }

                this.DataStatusNotify(currentState.Message, StatusCode.Failure);
                if (this.isStudent)
                {
                    this.isRunning = false;
                }
            }

            MastiDiagnostics.LogInfo(Helper.ContextLogger("Returned EndPoint" + Helper.GetEndPoint(socket), 5));
        }

        /// <summary>
        /// Callback for asynchronous send, if data is left to send, then send the remaining data, otherwise go into wait for acknowledgement state.
        /// </summary>
        /// <param name="ar">An IAsyncResult that references the asynchronous send.</param>
        private void SendCallback(IAsyncResult ar)
        {
            ClientState state = (ClientState)ar.AsyncState;
            int sentData = state.Socket.EndSend(ar, out SocketError socketError);

            MastiDiagnostics.LogInfo(Helper.ContextLogger(string.Format("BytesSent : {0}   TotalBytes: {1}", sentData, state.DataToSend().Length), 2));

            if (socketError != SocketError.Success)
            {
                MastiDiagnostics.LogError(Helper.ContextLogger("SocketError EndPoint :: " + Helper.GetEndPoint(state.Socket), 0));  // Socket Failed
                this.ResetClient(state.IPPort); // Remove any lock-objects,sockets,status bit associated to failed socket.
                lock (this.ackLock)
                {
                    this.acknowledgeEvents[Helper.MD5Hash(state.Message) + state.IPPort].Close();
                    this.acknowledgeEvents.Remove(Helper.MD5Hash(state.Message) + state.IPPort);
                }

                this.DataStatusNotify(state.Message, StatusCode.Failure);
                if (this.isStudent)
                {
                    this.isRunning = false;
                }

                return;
            }

            state.DataSent += sentData;

            if (state.DataSent != state.DataToSend().Length)
            {   // not all data was sent so send remaining bytes
                state.Socket.BeginSend(
                             state.DataToSend(), state.DataSent, state.DataToSend().Length - state.DataSent, SocketFlags.None, new AsyncCallback(this.SendCallback), state);
            }
            else
            {
                if (state.DataToSend()[4] == 0)
                {
                    MastiDiagnostics.LogSuccess(Helper.ContextLogger(string.Format("EndPoint {0} Message Sent :: {1}", Helper.GetEndPoint(state.Socket), Helper.ShortLog(state.Message)), 1));
                }
                else
                {
                    MastiDiagnostics.LogSuccess(Helper.ContextLogger(string.Format("Acknowledgement EndPoint {0} Message Sent :: {1}", Helper.GetEndPoint(state.Socket), state.Message), 2));
                }

                // Now that all data has been sent we release the lock.
                this.ReleaseLock(state.IPPort);
                if (state.DataToSend()[4] == 0)
                {
                    // Can maybe improved See TODO of waitForAcknowledgement declaration.
                    new Thread(() => this.WaitForAcknowledgement(state.IPPort, state.Message, state.Tries + 1))
                    {
                        IsBackground = true,
                        Name = Thread.CurrentThread.Name + " # Target IP : " + Helper.GetEndPoint(state.Socket)
                    }.Start();
                }
            }
        }

        /// <summary>
        /// Goes into wait for acknowledgment for a given message,IP 
        /// If acknowledgement is received within given time it calls DataStatusNotify
        /// otherwise it retries.
        /// if max-retries are exceeded it calls DataStatusNotify with failure.
        /// </summary>
        /// <param name="ipPort">The IP:Port Corresponding to the client it is waiting for.</param>
        /// <param name="message">Message For which it is waiting</param>
        /// <param name="retries">How many tries have already been made to send above data</param>
        private void WaitForAcknowledgement(IPEndPoint ipPort, string message, int retries)
        {
            string key = Helper.MD5Hash(message) + ipPort;

            AutoResetEvent ackEvent = this.acknowledgeEvents[key];
            try
            {
                if (ackEvent.WaitOne(this.waitTimeForAcknowledge))
                {
                    lock (this.ackLock)
                    {
                        this.acknowledgeEvents[key].Close();
                        this.acknowledgeEvents.Remove(key);
                    }

                    MastiDiagnostics.LogWarning(Helper.ContextLogger("Calling StatusNotify With success for " + Helper.ShortLog(message), 1));
                    this.DataStatusNotify(message, StatusCode.Success);
                    return;
                }
            }
            catch (ObjectDisposedException)
            {
                MastiDiagnostics.LogError(Helper.ContextLogger("Object was already Diposed. The Main thread may have ended", 1));
                return;
            }

            MastiDiagnostics.LogWarning(Helper.ContextLogger("Not Received Acknowledgement for " + Helper.ShortLog(message), 1));
            lock (this.ackLock)
            {
                this.acknowledgeEvents[key].Close();
                this.acknowledgeEvents.Remove(key);
            }

            if (retries < this.maxTries)
            {
                Socket refreshSocket = this.connectedClients[ipPort.Address];
                this.SendHelper(message, retries, refreshSocket, 0);
            }
            else
            {
                this.DataStatusNotify(message, StatusCode.Failure);
                if (this.isStudent)
                {
                    this.isRunning = false;
                }
            }
        }

        /// <summary>
        /// Starts a connection from student's machine to professor
        /// </summary>
        /// <returns> true if successful connection was made otherwise false.</returns>
        private bool InitiateConnection()
        {
            MastiDiagnostics.LogInfo(Helper.ContextLogger(string.Format("Creating connection to professor IP {0} Port {1}", this.ipAddress, this.port), 1));

            TcpClient tcpClient = new TcpClient();

            // We bind the socket manually, because otherwise the OS would automatically choose a IP/Interface.
            if (this.testIP != null)
            {
                tcpClient.Client.Bind(new IPEndPoint(this.testIP, 0));
            }

            try
            {
                tcpClient.Connect(this.ipAddress, this.port);
            }
            catch (SocketException e)
            {
                MastiDiagnostics.LogError(Helper.ContextLogger($"General : Unable to create connection with professor Error:{e.ToString()}", 0));
                return false;
            }

            Socket newSocket = tcpClient.Client;
            IPEndPoint endPoint = (IPEndPoint)newSocket.RemoteEndPoint;

            // Start a receiving thread for the student.
            Thread receivingThread = new Thread(() => this.HandleClientRequest(tcpClient));
            receivingThread.IsBackground = true;

            // Below method polls a socket to tell if it's writable.
            if (newSocket.Poll(-1, SelectMode.SelectWrite))
            {
                MastiDiagnostics.LogInfo(Helper.ContextLogger(string.Format("This Socket {0} is writable.", endPoint), 5));
                receivingThread.Start();
                lock (this.mainLock)
                {
                    if (!this.lockObjects.ContainsKey(endPoint)) 
                    {
                        this.lockObjects.Add(endPoint, new object());
                    }

                    if (!this.lockStatus.ContainsKey(endPoint)) 
                    {
                        this.lockStatus.Add(endPoint, false);
                    }

                    this.connectedClients.Add(endPoint.Address, newSocket);
                }

                this.isRunning = true;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Remove Locks,Status and objects corresponding to an Client
        /// </summary>
        /// <param name="ipPort">IP:Port for which locks are to be removed.</param>
        private void ResetClient(IPEndPoint ipPort)
        {
            lock (this.mainLock)
            {
                this.connectedClients.Remove(ipPort.Address);
                this.ReleaseLock(ipPort);
                this.lockStatus.Remove(ipPort);
                this.lockObjects.Remove(ipPort);
            }
        }

        /// <summary>
        /// Grants a Lock to a thread . There exists 1 lock/Client.
        /// </summary>
        /// <param name="ipPort">EndPoint for which lock is needed.</param>
        /// <returns>true if lock is granted.</returns>
        private bool GetLock(IPEndPoint ipPort)
        {
            try
            {
                if (this.lockStatus[ipPort] != true)
                {
                    lock (this.lockObjects[ipPort])
                    {
                        if (this.lockStatus[ipPort] != true)
                        {
                            lock (this.mainLock)
                            {
                                this.lockStatus[ipPort] = true;
                            }

                            return true;
                        }
                    }
                }
            }
            catch (KeyNotFoundException)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Release the lock corresponding to an client.
        /// </summary>
        /// <param name="ipPort">Endpoint for which lock is to be released</param>
        private void ReleaseLock(IPEndPoint ipPort)
        {
            lock (this.mainLock)
            {
                if (this.lockStatus.ContainsKey(ipPort))
                {
                    this.lockStatus[ipPort] = false;
                }
            }
        }

        /// <summary>
        /// Updates the acknowledgeEvents table to tell that an acknowledgement message has been received.
        /// </summary>
        /// <param name="key">Key corresponding to the message to be acknowledged.</param>
        private void Acknowledge(string key)
        {
            MastiDiagnostics.LogInfo(Helper.ContextLogger("Received Acknowledgement for HASH " + key, 2));
            lock (this.ackLock) 
            {
                if (this.acknowledgeEvents.ContainsKey(key))
                {
                    MastiDiagnostics.LogInfo(Helper.ContextLogger("Received Acknowledgement for HASH " + key + " was valid ", 1));
                    this.acknowledgeEvents[key].Set();
                }
                else
                {
                    this.waitTimeForAcknowledge += 1000;
                }
            }
        }
    }
}