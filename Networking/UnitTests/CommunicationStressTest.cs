﻿// -----------------------------------------------------------------------
// <author> 
//      Rohith Reddy G
// </author>
//
// <date> 
//      28/10/2018
// </date>
// 
// <reviewer>
//      Libin, Parth, Jude
// </reviewer>
//
// <copyright file="CommunicationStressTest.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      this file is used to do stress testing of the networking module
//      send function is used to send data to a given server multiple times 
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// This class executes stress testing 
    /// </summary>
    public class CommunicationStressTest : ITest
    {
        /// <summary>
        /// event to pause the thread from executing
        /// </summary>
        private static ManualResetEvent manualResetProf = new ManualResetEvent(false);

        /// <summary>
        /// event to pause the thread from executing
        /// </summary>
        private static ManualResetEvent manualResetStud = new ManualResetEvent(false);

        /// <summary>
        /// schema module instance
        /// </summary>
        private ISchema schema = new MessageSchema();

        /// <summary>
        /// return value of the bool Run function
        /// </summary>
        private bool profRetval = true;

        /// <summary>
        /// return value of the bool Run function
        /// </summary>
        private bool studRetval = true;

        /// <summary>
        /// n reprsents the no of times the send function is used
        /// </summary>
        private int n = 10;

        /// <summary>
        /// boolean array stores the bool values
        /// </summary>
        private bool[] profBoolean = new bool[100];

        /// <summary>
        /// boolean array stores the bool values
        /// </summary>
        private bool[] studBoolean = new bool[100];

        /// <summary>
        /// pointer to check the data status
        /// </summary>
        private int profPointer = 0;

        /// <summary>
        /// pointer to check the data status
        /// </summary>
        private int studPointer = 0;

        /// <summary>
        /// message to be sent 
        /// </summary>
        private string message = "hi";

        /// <summary>
        /// ip address of the machine to which data is sent 
        /// </summary>
        private IPAddress ip = IPAddress.Parse("169.254.25.29");

        /// <summary>
        /// Initializes a new instance of the <see cref="CommunicationStressTest" /> class.
        /// </summary>
        /// <param name="logger">Logger object used for logging during test</param>
        public CommunicationStressTest(ILogger logger)
        {
            this.Logger = logger;
        }

        /// <summary>
        /// Gets or sets Logger instance.
        /// </summary>
        public ILogger Logger { get; set; }

        /// <summary>
        /// This function is the part of ITest interface from where the execution starts 
        /// </summary>
        /// <returns>true or false</returns>
        public bool Run()
        {
            using (Communication profCommunication = new Communication(8010, 3, 1))
            {
                using (Communication studentCommunication = new Communication(IPAddress.Parse(profCommunication.LocalIP), 8010, 3, 1))
                {
                    this.ip = IPAddress.Parse(profCommunication.LocalIP);
                    /* subscribe for data receival */
                    profCommunication.SubscribeForDataReceival(DataType.Message, this.ProfDataReceival);

                    /* subscribe for data receival */
                    studentCommunication.SubscribeForDataReceival(DataType.Message, this.StudDataReceival);


                    for (int i = 0; i < this.n; i++)
                    {
                        /* message dict */
                        Dictionary<string, string> messageDict = new Dictionary<string, string>();

                        /* message added to dict */
                        messageDict.Add("message", this.message);
                        messageDict.Add("index", i.ToString(CultureInfo.CurrentCulture));

                        /* packet to be sent */
                        string packet = this.schema.Encode(messageDict);
                        /* send function */
                        studentCommunication.Send(packet, this.ip, DataType.Message);
                    }

                    manualResetProf.WaitOne();

                    this.profRetval = this.profBoolean[0];
                    for (int i = 1; i < this.n; i++)
                    {
                        this.profRetval = this.profRetval && this.profBoolean[i];
                    }

                    for (int i = 0; i < this.n; i++)
                    {
                        /* message dict */
                        Dictionary<string, string> messageDict = new Dictionary<string, string>();

                        /* message added to dict */
                        messageDict.Add("message", this.message);
                        messageDict.Add("index", i.ToString(CultureInfo.CurrentCulture));

                        /* packet to be sent */
                        string packet = this.schema.Encode(messageDict);
                        /* send function */
                        profCommunication.Send(packet, this.ip, DataType.Message);
                    }
                    
                    manualResetStud.WaitOne();

                    this.studRetval = this.studBoolean[0];
                    for (int i = 1; i < this.n; i++)
                    {
                        this.studRetval = this.studRetval && this.studBoolean[i];
                    }
                }
            }

            return this.profRetval && this.studRetval;
        }

        /// <summary>
        /// This function is called when a message is received
        /// We can extract the ip and data from this function 
        /// </summary>
        /// <param name="data">message data</param>
        /// <param name="fromIP">ip of the sender</param>
        private void ProfDataReceival(string data, IPAddress fromIP)
        {
            IDictionary<string, string> messageDict = this.schema.Decode(data, false);
            if (messageDict["message"] == this.message)
            {
                this.profBoolean[this.profPointer] = true;
                this.Logger.LogSuccess("message sent ");
            }
            else
            {
                this.profBoolean[this.profPointer] = false;
                this.Logger.LogError("error in sending message");
            }

            this.profPointer++;

            if (this.profPointer >= this.n)
            {
                manualResetProf.Set();
            }
        }

        /// <summary>
        /// This function is called when a message is received
        /// We can extract the ip and data from this function 
        /// </summary>
        /// <param name="data">message data</param>
        /// <param name="fromIP">ip of the sender</param>
        private void StudDataReceival(string data, IPAddress fromIP)
        {
            IDictionary<string, string> messageDict = this.schema.Decode(data, false);
            if (messageDict["message"] == this.message)
            {
                this.studBoolean[this.studPointer] = true;
                this.Logger.LogSuccess("message sent");
            }
            else
            {
                this.studBoolean[this.studPointer] = false;
                this.Logger.LogError("error in sending message");
            }

            this.studPointer++;

            if (this.studPointer >= this.n)
            {
                manualResetStud.Set();
            }
        }
    }
}
